import pickle
import pandas as pd
import numpy as np
from sklearn import preprocessing


class My_Model:


	def __init__(self):

		self.file_name = 'final_model_lr.pkl' # Logistic Regression Model


	def add_cols(self, dataframe):

		# Adding duration_months
		start = pd.to_datetime(dataframe['launched'].apply(lambda x: x.split(" ")[0]))
		end = pd.to_datetime(dataframe['deadline'])
		months = ((end-start)/np.timedelta64(1,'M') + 1).astype(int)
		dataframe = dataframe.assign(duration_months = months)

		# Adding pledge_goal_ratio
		pledge = dataframe.usd_pledged_real
		goal = dataframe.usd_goal_real
		ratio = pledge/goal
		dataframe = dataframe.assign(pledge_goal_ratio = ratio)
		return dataframe

	def get_features(self,dataframe,features):
		new_df = dataframe.loc[:,features]
		return new_df
		
	def encode_features(self,dataframe,features):
		for f in features:
			le = preprocessing.LabelEncoder()
			le = le.fit(dataframe[f])
			dataframe[f] = le.transform(dataframe[f])
		return dataframe

	def prepare_dataset(self,dataframe):

		features = ['category','main_category','duration_months','usd_goal_real','usd_pledged_real','pledge_goal_ratio','backers','country']
		dataframe = self.add_cols(dataframe)
		dataframe = self.get_features(dataframe,features)
		dataframe = self.encode_features(dataframe,features)
		return dataframe

	
	def load_model(self):

		new_model = pickle.load(open(self.file_name,'rb'))
		return new_model


	def predict_state(self,model,dataframe):

		predictions = model.predict(dataframe)
		print(predictions)





