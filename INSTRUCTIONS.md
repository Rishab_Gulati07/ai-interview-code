# Instructions for running the code

My solution consists of 4 parts:

1. Kickstarter_Analysis.ipynb : This consists of all the preprocessing, exploratory analysis and experimentation with different models.
2. final_model_lr.pkl : This is the pickle file storing the trained model with highest test accuracy.
3. model_class.py :  This is the Python Module that you can import in order to test the model.
4. Sample_Run.ipynb : This is a try run of how to import the model and use it to make predictions. 

## How to use model_class.py

1. First import the file.
2. Create an instance of the class. For example: model = model_class.My_Model()
3. Get the dataframe using pandas.
4. Use the prepare_dataset function from the My_Model class to remove unnecessary columns, add required columns and encode the dataset. Eg: df = model.prepare_dataset(df). The parameter here is the dataframe to be tested.
5. Load the model using load_model function of the class. Eg: new_model = model.load_model()
6. Print the predictions by using predict_state function of the class. The parameters are the loaded model and the dataframe on which prediction is to be performed.
	 Eg: model.predict_state(new_model,df)